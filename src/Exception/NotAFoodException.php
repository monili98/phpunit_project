<?php

namespace App\Exception;

use Exception;

class NotAFoodException extends \Exception
{
    protected $message = "Please don't add different type products...";
}