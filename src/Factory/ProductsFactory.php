<?php

namespace App\Factory;

use App\Entity\Product;

class ProductsFactory
{
    public function addProduct(string $name, int $price): Product
    {
        $product = new Product();
        $product->setName($name);
        $product->setPrice($price);

        return $product;
    }

    public function addProductFromString(string $string): Product
    {
        $spec_array = explode(" ", $string);
        $product = new Product();
        if(count($spec_array) >= 1)
        {
            if(is_string($spec_array[0]) && is_int((int)$spec_array[1]))
            {
                $product->setName($spec_array[0]);
                $product->setPrice((int)$spec_array[1]);
            }
        }

        return $product;
    }

    public function addBed(int $price): Product
    {
        $product = new Product('Bed', $price, false);
        return $product;
    }

    public function checkParamType(int $param): Product
    {
        return $param * 3;

        $product = new Product();
        $product->setName('Lazi');
        $product->setPrice($param);

        return $product;
    }
}