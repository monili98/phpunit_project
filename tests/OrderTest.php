<?php

namespace App\Tests;

use App\Entity\PaymentGateway;
use App\Entity\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    /**
     * @var |PHPUnit_Framework_MockObject_MockObject
     */
    private $gateway;
    private $order;

    public function setUp(): void
    {
        // $gateway = new PaymentGateway; //This class doesn't exist, so we create mock instead
        $this->gateway = $this->createMock(PaymentGateway::class);
        $this->order = new Order($this->gateway); //passing mock object
    }

    public function testOrderIsProcessed()
    {
        $this->order->amount = 200;

        $this->gateway->method('charge')->willReturn(20); //to change mocked class method return value (default is null or 0)

        $processedValue = $this->order->process();
        $this->assertEquals(20, $processedValue);
    }

    /**
     * @dataProvider chargeMethodParameterTests
     */
    public function testChargeMethodParamType(int $amount)
    {
        $this->gateway
            ->expects($this->once()) //charge method should be called once
            ->method('charge')
            ->with($amount) //method should be called only with this value; charge(10) doesn't allow
            ->willReturn(10); //to change mocked class method return value (default is null or 0)

        $processedValue = $this->gateway->charge($amount);
//        $processedValue = $this->gateway->charge(10);
        $this->assertEquals(10, $processedValue);
    }

    public function chargeMethodParameterTests()
    {
        return [ //test case array
            'first try' => [5], //we can assign key to know which test case failed
            'second try' => [6],
            'third try' => [12]
        ];
    }
}