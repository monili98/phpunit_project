<?php

namespace App\Entity;

use App\Exception\NotAFoodException;
use App\Exception\SecurityDoNotWorkException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Product;

/**
 * @ORM\Entity
 * @ORM\Table(name="enclosure")
 */
class Enclosure
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="enclosure", cascade="persist")
     */
    private $products;

    //Collection of security objects
    /**
     * @var Collection|Security[]
     * @ORM\OneToMany(targetEntity="App\Entity\Security", mappedBy="enclosure", cascade="persist")
     */
    private $securities; //if collection is empty, there is no security

    /**
     * @param $products
     */
    public function __construct(bool $withBasicSecurity = false)
    {
        $this->products = new ArrayCollection();
        $this->securities = new ArrayCollection();
        if ($withBasicSecurity)
        {
            $this->addSecurity(new Security('Security_1', true, $this));
        }
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product)
    {
        if (!$this->canAddProduct($product))
        {
            throw new NotAFoodException();
        }
        if (!$this->isSecurityActive())
        {
            throw new SecurityDoNotWorkException("Security is not working now, please do not add new products");
        }
        $this->products[] = $product;
    }

    public function addSecurity(Security $security)
    {
        $this->securities[] = $security;
    }

    public function isSecurityActive() :bool
    {
        foreach ($this->securities as $security) //go throw security array and search for even one active security
        {
            if ($security->getIsActive())
            {
                return true;
            }
        }
        return false; //if there is no one active security
    }

    public function getSecurities(): Collection
    {
        return $this->securities;
    }

    private function canAddProduct(Product $product) :bool
    {
        return count($this->products) === 0 || $this->products->first()->getIsFood() === $product->getIsFood(); //if enclosure is empty or has one product same type we want to add
    }
}