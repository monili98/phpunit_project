<?php

namespace App\Tests\Servise;

use App\Entity\Enclosure;
use App\Entity\Product;
use App\Factory\ProductsFactory;
use App\Service\EnclosureBuilderService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class EnclosureBuilderServiceTest extends TestCase
{
    public function testItBuildsAndPersistsEnclosure()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->once())
            ->method(('persist'))
            ->with($this->isInstanceOf(Enclosure::class));

        $em->expects($this->atLeastOnce())
            ->method('flush');

        $productsFactory = $this->createMock(ProductsFactory::class);

        $productsFactory->expects($this->exactly(2)) // call method two times
            ->method('checkParamType') // method name
            ->willReturn(new Product())
            ->with($this->isType('int')); // type of param type

        $productsFactory->checkParamType(5);
        $productsFactory->checkParamType(6);

        $builder = new EnclosureBuilderService($em, $productsFactory);
        $enclosure = $builder->buildEnclosure(2, 5);

        $this->assertCount(2, $enclosure->getSecurities());
        $this->assertCount(5, $enclosure->getProducts());
    }
}