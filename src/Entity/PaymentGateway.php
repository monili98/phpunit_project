<?php

namespace App\Entity;

class PaymentGateway
{
    public function charge(int $amount): int
    {
        return $amount * 3;
    }

}