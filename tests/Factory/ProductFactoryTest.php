<?php

namespace App\Tests\Factory;

use App\Entity\Product;
use App\Service\ProductNameLengthDeterminator;
use PHPUnit\Framework\TestCase;
use App\Factory\ProductsFactory;

class ProductFactoryTest extends TestCase
{
    /**
     * @var ProductsFactory
     */
    private $factory;

    public function setUp(): void //this method will be called before every test method
    {
        $mockLengthDeterminator = $this->createMock(ProductNameLengthDeterminator::class);
        // mocking - to create and pass object which is very similar to real, but isn't. While mocking, we create real objects but with fake methods
        // overrides all class methods but returns null
        $this->factory = new ProductsFactory($mockLengthDeterminator);

    }

    public function testProductsFactory()
    {
//        $products = new ProductsFactory();
        $product = $this->factory->addProduct('Bed', 160);

        $this->assertInstanceOf(Product::class, $product);

    }

    public function testProductsIncomplete()
    {
        $this->markTestIncomplete('Waiting for test confirmation...');
    }

    public function testProductsSkipped()
    {
        if (!class_exists('Nanny'))
        {
            $this->markTestSkipped('Test is skipped.');
        }
        $product = $this->factory->addProduct('Bed', 160);
        $this->assertInstanceOf(Product::class, $product);
    }

    public function testExpensiveProducts()
    {
        $product = $this->factory->addProduct('Chair', 260);
        $this->assertGreaterThanOrEqual(Product::EXPENSIVE, $product->getPrice());
    }

    /**
     * @dataProvider getFromStringTest
     */
    public function testAddProductFromString(string $spec) //Test input as parameter
    {
        $product = $this->factory->addProductFromString($spec);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertSame('Chair', $product->getName());
        $this->assertSame(1400, $product->getPrice());
    }

    //To test same test with different values
    //Two tests will fail and one pass
    public function getFromStringTest()
    {
        return [ //test case array
            'first try' => ['Chair 1400'], //we can assign key to know which test case failed
//            'second try' => ['Bed 1500'],
//            'third try' => ['Table 3000']
        ];
    }

        /**
         * @dataProvider getCheapProductFromStringTests
         */
        public function testCheapProductFromString(string $spec)
        {
            $product = $this->factory->addProductFromString($spec);
            self::assertLessThanOrEqual(Product::CHEAP, $product->getPrice());
        }

        public function getCheapProductFromStringTests()
        {
            return [
                'first try' => ['Chair 14'],
    //            'second try' => ['Bed 22'],
                'third try' => ['Table 2']
            ];
        }

    /**
     * @dataProvider addBedMockTests
     */
    public function testAddBedProductMock(int $price) //Test input as parameter
    {
        $product = $this->factory->addBed($price);
        $this->assertSame($price, $product->getPrice());
    }

    //To test same test with different values
    //Two tests will fail and one pass
    public function addBedMockTests()
    {
        return [ //test case array
            'first try' => [5], //we can assign key to know which test case failed
            'second try' => [6],
            'third try' => [12]
        ];
    }






}