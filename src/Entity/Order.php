<?php

namespace App\Entity;

class Order
{
    //We do not have PaymentGateway class, so we need to do mocking

    /**
     * @var int
     */
    public $amount = 0;

    /**
     * @var PaymentGateway
     */
    protected $gateway;

    /**
     * Constructor
     * @return void
     */
    public function __construct(PaymentGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * Process the order
     * @return int
     */
    public function process()
    {
        return $this->gateway->charge($this->amount);
    }
}