<?php

namespace App\Service;

use App\Entity\Product;
use App\Factory\ProductsFactory;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Enclosure;
use App\Entity\Security;

class EnclosureBuilderService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ProductsFactory
     */
    private $productsFactory;

    private $securities;
    private $products;

    public function __construct(EntityManagerInterface $entityManager, ProductsFactory $productsFactory)
    {
        $this->entityManager = $entityManager; // entity manager requires connecting to database, we don't want to do it, so we will use mocking
        $this->productsFactory = $productsFactory;
    }

    public function buildEnclosure(int $numberOfSecuritySystems = 1, int $numberOfProducts = 3): Enclosure
    {
        $enclosure = new Enclosure();

        $this->addSecuritySystems($numberOfSecuritySystems, $enclosure);

        $this->addProducts($numberOfProducts, $enclosure);

        $this->entityManager->persist($enclosure);
        $this->entityManager->flush($enclosure);

        return $enclosure;
    }

    private function addSecuritySystems(int $numberOfSecuritySystems, Enclosure $enclosure)
    {
        $securityNames = ['SecurityName1', 'SecurityName2', 'SecurityName3'];
        for ($i = 0; $i < $numberOfSecuritySystems; $i++) {
            $securityName = $securityNames[array_rand($securityNames)];
            $security = new Security($securityName, true, $enclosure);

            $enclosure->addSecurity($security);
        }
    }

    private function addProducts(int $numberOfProducts, Enclosure $enclosure)
    {
        $names = ['bed', 'curtains', 'table'];
        $available_prices = [4, 8, 12, 20, 50, 150, 8];

        for ($i = 0; $i < $numberOfProducts; $i++) {
            $product = new Product('bed', array_rand($available_prices), false, $enclosure);

            $enclosure->addProduct($product);
        }
    }
}