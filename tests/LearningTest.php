<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Product;
use App\Repository\ProductRepository;

class LearningTest extends TestCase
{
    public function testWorking()
    {
        $this->assertTrue(true);
    }

    public function testProductNameLength()
    {
        $product = new Product();
        $this->assertSame(0, $product->getNameLength()); //expected value, actual value
        //assertSame also check if the type match, assertEquals doesn't do this

        $product->setName("Chair");
        $this->assertSame(5, $product->getNameLength());
    }

    public function testProductPrice()
    {
        $product = new Product();
        $product->setPrice(115);
        self::assertGreaterThan(100, $product->getPrice(), '---Product price is lower than 100, it suppose to be bigger than 100'); //We can add additional message on test fail
    }

    //UNIT TEST - To test one specific function on a class; Fake any needed dataabse connections
    //INTEGRATION TEST - Like Unit test, except it uses the real database connection
    //FUNCTIONAL TEST - To write a test to programatically command a browser

    public function testReturnsFullNameOfProduct()
    {
        $product = new Product();
        $product->setName('Table');
        $this->assertSame('Table', $product->getName());
    }

    public function testType()
    {
        $product = new Product();
        $product->setName('Curtains');
        $product->setPrice(78);
//        $product = 5;
        $this->assertInstanceOf(Product::class, $product);
        $this->assertIsString($product->getName()); //since PHPUnit8 we don't use assertInternalType
        $this->assertIsInt($product->getPrice());
    }
}