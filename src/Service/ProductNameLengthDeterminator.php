<?php

namespace App\Service;

use App\Entity\Product;

class ProductNameLengthDeterminator
{
    public function getProductNameLengthFromSpecString(string $string): int
    {
        $spec_array = explode(" ", $string);
        if(count($spec_array) >= 1)
        {
            if(is_string($spec_array[0]) && is_int((int)$spec_array[1]))
            {
                return strlen($spec_array[0]);
            }
        }
        return 0;
    }

    public function addProductFromString(string $string): Product
    {
        $spec_array = explode(" ", $string);
        $product = new Product();
        if(count($spec_array) >= 1)
        {
            if(is_string($spec_array[0]) && is_int((int)$spec_array[1]))
            {
                $product->setName($spec_array[0]);
                $product->setPrice((int)$spec_array[1]);
            }
        }

        return $product;
    }
}