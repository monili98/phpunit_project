<?php

namespace App\Tests\Servise;

use App\Service\ProductNameLengthDeterminator;
use PHPUnit\Framework\TestCase;

class ProductNameLengthDeterminatorTest extends TestCase
{
    /**
     * @dataProvider getSpecLengthTests
     */
    public function testItReturnsCorrectLengthInRange($spec, $minLength, $maxLength)
    {
        $determinator = new ProductNameLengthDeterminator();
        $actualSize = $determinator->getProductNameLengthFromSpecString($spec);

        $this->assertGreaterThanOrEqual($minLength, $actualSize); //second parameter should be greater than first parameter, or equal to.
        $this->assertLessThanOrEqual($maxLength, $actualSize);
    }

    public function getSpecLengthTests()
    {
        return [
            // specificationsAsString, minLength, maxLength
            ['Bed 100 false', 2, 400],
            ['Pencil 5 false', 2, 10],
            ['Notebook 12 false', 4, 40],
        ];
    }
}