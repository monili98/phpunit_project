<?php

namespace App\Entity;

use App\Entity\Enclosure;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    const EXPENSIVE = 200;
    const CHEAP = 15;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFood;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Enclosure", inversedBy="products")
     */
    private $enclosure;

    /**
     * @param $name
     * @param $price
     * @param $isFood
     */
    public function __construct(?string $name = null, ?int $price = null, ?bool $isFood = null, Enclosure $enclosure = null)
    {
        $this->name = $name;
        $this->price = $price;
        $this->isFood = $isFood;
        $this->enclosure = $enclosure;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsFood(): bool
    {
        return $this->isFood;
    }

    public function setIsFood(bool $isFood): self
    {
        $this->isFood = $isFood;

        return $this;
    }

    public function getNameLength(): int
    {
        return strlen($this->name);
    }
}
