<?php

namespace App\Tests;

use App\Exception\SecurityDoNotWorkException;
use PHPUnit\Framework\TestCase;
use App\Entity\Enclosure;
use App\Entity\Product;
use App\Exception\NotAFoodException;

class EnclosureTest extends TestCase
{
    public function testItHasNoProductsByDefault()
    {
        $enclosure = new Enclosure();
        $this->assertCount(0, $enclosure->getProducts());
    }

    public function testItAddsProducts()
    {
        $enclosure = new Enclosure(true);
        $enclosure->addProduct(new Product('Table', 50, false));
        $enclosure->addProduct(new Product('Table', 50, false));

        $this->assertCount(2, $enclosure->getProducts());
    }

    public function testItDoesNotAllowToAddDifferentTypeProduct() //pass if throw exception
    {
        $enclosure = new Enclosure(true);
        $enclosure->addProduct(new Product('Table', 50, false));

        $this->expectException(NotAFoodException::class); //this should be before final code (asserting that NotAFoodException will be thrown)

        $enclosure->addProduct(new Product('Chair', 50, true));
        //throws exception when in enclosure exists products and we want to add new product, different type(isFood) than first product in enclosure
    }
    /**
     * @expectException(NotAFoodException)
     */
    public function testItDoesAllowToAddSameTypeProduct() //pass if not throw exception
    {
        $enclosure = new Enclosure(true); //to create enclosure with basic security
        $enclosure->addProduct(new Product('Table', 50, false));
        $enclosure->addProduct(new Product('Chair', 50, false));
    }

    public function testItDoesNotAllowToAddProductsToUnsecureStorage()  //pass if throw exception
    {
        $enclosure = new Enclosure(); //to create enclosure without security
        $this->expectException(SecurityDoNotWorkException::class);
        $this->expectExceptionMessage("Security is not working now, please do not add new products"); //Exception messages there and in class method should match

        $enclosure->addProduct(new Product()); //in method addProduct we try to throw exception
    }
}